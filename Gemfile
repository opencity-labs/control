source "https://rubygems.org"

ruby "3.2.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.1.1"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem "sprockets-rails"

# Use postgresql as the database for Active Record
gem "pg", "~> 1.1"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma", ">= 5.0"

# Use JavaScript with ESM import maps [https://github.com/rails/importmap-rails]
gem "importmap-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Bundle and process CSS [https://github.com/rails/cssbundling-rails]
gem "cssbundling-rails"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production
# gem "redis", ">= 4.0.1"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[ windows jruby ]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

# Figaro
# https://github.com/laserlemon/figaro
# have settings in config/application.yml
gem 'figaro'


group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[ mri windows ]

  # https://docs.rubocop.org/rubocop-rails/installation.html
  # gem 'rubocop-ast', '~> 0.7.1', require: false
  gem 'rubocop-ast', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false

  gem 'byebug'

  # to enable ruby syntaxy tools
  gem "standard"

  # https://github.com/troessner/reek
  # detect code smell
  # run with `reek`
  gem 'reek'

  # https://github.com/gregnavis/active_record_doctor
  #  helps to keep the database in a good shape
  #  run with `bundle exec rake active_record_doctor`
  gem 'active_record_doctor'

end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"

  # https://github.com/guard/listen
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen'

  # better errors displayed in development
  # https://github.com/charliesome/better_errors
  gem "better_errors"
  # suggested by  "better_errors"
  gem "binding_of_caller"

  # BEST for developing: add footnotes with useful data
  # https://github.com/josevalim/rails-footnotes
  # https://dirk.net/2010/04/28/using-rails-footnotes-with-vimgvim/
  # this does not work anymore, see https://github.com/indirect/rails-footnotes/issues/153
  # gem 'rails-footnotes', '~> 4.0'

  # https://github.com/presidentbeef/brakeman for security
  gem 'brakeman'

  # https://github.com/DamirSvrtan/fasterer
  gem 'fasterer'

  # https://github.com/flyerhzm/rails_best_practices
  gem 'rails_best_practices'

  # https://medium.com/modern-rails/how-to-use-the-annotate-gem-c44bfec97d03
  # triggered with rake db:migrate, options in lib/tasks/auto_annotate_models.rake
  gem 'annotate'

  # https://github.com/flyerhzm/bullet
  # spotting N+1 queries
  # to ENABLE modify config/environments/development.rb
  gem 'bullet'

end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara"
  gem "selenium-webdriver"
end
