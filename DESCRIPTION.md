# Project description

We need a simple web tool to guide our Customers when they have to point their domain
to our servers.

A Customer typically would need to create/modify two DNS records of their domain: this app
guide customer to know exactly what records need to be modified, and to what value they need
to be assigned.

The app will also check those DNS values, and show it they have been assignd correctly.

## Details

As a Saas provider we offer several platforms, each of it has its own DNS settings.

Once the **platform** and the **domain** is chosen, this app will:
* show DNS records that needs to be created
* check DNS records so to notify a user when these records are correctly set


#### DNS Content


A saas platform will tipically have:
* one (or more) IP addresses:  `IP1`, `IP2`
* one CNAME: `xxx.yyy.com`

Typically that CNAME will resolve to the above two IP addresses

Given a domain `mytown.mydomain.it` this is a typical DNS record setting:

```
TYPE    NAME                    CONTENT
-------------------------------------------
A       mytown.mydomain.it      IP1
A       mytown.mydomain.it      IP2
CNAME   www.mytown.mydomain.it  xxx.yyy.com
```

So, a customer that want to have his domain in our saas platform will have
to set those 3 DNS records.

Note the `www.` version of the domain: while the version without www is often preferred, it is always a good practice to have both version of a domain:
with and without the `www.` prefix, for several reasons:
1. several domains still have or had in the past the version with `www`, and this version has been promoted, printed, cited... ecc, so it needs to be kept
1. nowadays browsers tend to hide the `www.` even if typed in, so a user surfing a website will not really know if
the domain he is surfing a domain with or without `www.`, so next time he may just use the version without `www.`


But it is also important that *only one version of the two* is the final URL destination: the other
one should just redirect to the main one: it is a personal choice to the customer which to prefer
but the version without `www` is shorter: easier to type in and to remember, so it should be preferred.


## Stateless application

This application is a Docker stateless app since there is no need to store new data: initial
settings is created at startup by populating a postgres database.


