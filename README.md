# ToolBox

Ruby on Rails application, created according
to https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/rails
with

```
rails new . --force --css=bootstrap --database=postgresql
```

## Getting started

Clone this repo, then set postgres password

```
echo "my-custom-password" > secrets/postgres_password.txt
```

then launch it with

```
docker compose build
docker compose up
```

and http://localhost:3000


## Entering Console

```
docker compose run --rm -it web bin/rails console
# or
docker compose run --rm -it web /bin/bash
> RAILS_ENV=production
> bin/rails console
```

or the running container with

```
docker exec  -it control-web-1 bin/rails console
```

## Rebuild the application

Changes to the Gemfile or the Compose file needs a rebuild.

Some changes require only

```
docker compose build
# or
docker compose up --build
```

but changes to `Gemfile` needs `bundle install` to sync changes to `Gemfile.lock`
or you'll see

```
ERROR:
You are trying to install in deployment mode after changing
your Gemfile. Run `bundle install` elsewhere and add the
updated Gemfile.lock to version control.
```

and this is due to `BUNDLE_DEPLOYMENT="1"` in Dockerfile, that assure
`Gemfile.lock` in this repo is the same used while running the app,
so the sync this has to be done from another
container (with the same ruby version) with

```
# create Gemfile.lock without installing
docker run --rm -v $(pwd):/app -w /app ruby:$(cat .ruby-version | cut -d "-" -f 2)-slim bundle lock --update
# followed by
docker compose up --build
```

## Database

Adminer at http://localhost:8080, or http://localhost:8080/?pgsql=db&username=postgres

Working with db

```
# first run (automatically done anyhow)
docker compose run --rm -it web bin/rails db:prepare
```

## Entering containers

Enter running container with

```
docker exec  -it control-web-1 /bin/bash
# or rails console with
docker exec  -it control-web-1 bin/rails console
```

or spin another container with:

```
docker run --rm -it control-web /bin/bash
```

To be able to run rails without compose, just entering a container, you need to add the RAILS_MASTER_KEY:

```
docker run --rm -it -p 3000:3000 --env RAILS_MASTER_KEY=`cat config/master.key` --env RAILS_ENV=development control-web bin/rails server --binding 0.0.0.0
```

## How to create a pristine new rails application

Follow https://semaphoreci.com/community/tutorials/dockerizing-a-ruby-on-rails-application
so with the file Dockerfile.rails, to be executed *outside* the rails tree of this repo.

```
docker build -t rails-toolbox -f Dockerfile.rails .
docker run -it -v $PWD:/opt/app rails-toolbox rails new --skip-bundle pristine_rails_app --force --css=bootstrap --database=postgresql
#
sudo chown -R $USER:$USER pristine_rails_app
rm -rf pristine_rails_app/.git
```

for a full check (bundle included) execute without the `--skip-bundle`

All files will refer to a new pristing rails 7 app: they can be copied in this repo

# Troubleshooting

For errors like

```
control-web-1      | /usr/local/bundle/ruby/3.2.0/gems/bootsnap-1.17.0/lib/bootsnap/compile_cache/iseq.rb:53:in `load_from_binary': invalid bytecode (RuntimeError)
```

just remove `/tmp` files: this script will leave the .keep ones

```
bin/my_tmp_directory_clean.sh
```
