#/bin/bash -e
#
# We check only committed files
FILES_TO_CHECK=`git ls-files --empty-directory | xargs`
command="docker run -ti --rm -v $(pwd):/work vlajos/misspell-fixer -runRVDs $FILES_TO_CHECK"
echo $command
$command

