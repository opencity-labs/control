#!/bin/bash

#echo "Rspec testing..."
#docker compose run -it web bash -c "bundle exec rspec"

#echo
#read -r -p "Press enter to continue"
#echo

echo "Rubocop check..."
docker compose run --rm -it web bash -c "bundle exec rubocop --fail-level warning --display-only-fail-level-offenses"

echo
read -r -p "Press enter to continue"
echo

echo "Brakeman security check..."
docker compose run --rm -it web bash -c "bundle exec brakeman --run-all-checks"

# no need after brakeman
# read -r -p "Press enter to continue"

# reek
# docker compose run --rm -it web bash -c "bundle exec reek app"
# read -r -p "Press enter to continue"


# echo "Fasterer check..."
# docker compose run --rm -it web bash -c "bundle exec fasterer"

#read -r -p "Press enter to continue"
#echo

# rails_best_practices .

# read -r -p "Press enter to continue"

# Useful, but not working now
# https://github.com/gregnavis/active_record_doctor
#echo "active_record_doctor"
#bundle exec rake active_record_doctor

echo
read -r -p "Press enter to continue"
echo
echo "misspell-fixer"
echo
FILES_TO_CHECK=`git ls-files --empty-directory | xargs`
docker run -ti --rm -v $(pwd):/work vlajos/misspell-fixer -runRVDs $FILES_TO_CHECK

echo
read -r -p "Press enter to continue"
echo

echo "Done!"
echo
echo "   Perfection is achieved, not when there is nothing more to add,"
echo "   but when there is nothing left to take away."
echo "                                        Antoine de Saint-Exupéry"
echo
