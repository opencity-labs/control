#/bin/bash
#
# Here, the -d option tells git clean to also remove untracked directories,
#           -f forces the operation,
#           -x removes ignored files too, and /tmp is the path to the directory to be cleaned.
#           -e option followed by the path to each file tells git clean to exclude those files from the operation.
git clean -dfx -e tmp/pids/.keep -e tmp/storage/.keep tmp
