# Basic controller


```
docker compose run --rm -it web bin/rails g controller home index
```


##  Models

We have many saas platforms, aka `Plaform`

Each platform has a CNAME records, which points to two or more IP addresses


```
rails generate scaffold Platform \
  name:string  \
  cname:string  \
  --no-timestamps

```
